using NUnit.Framework;

namespace appiumhb.Tests.Cases
{
    using Appiumhb.Main.Resources;
    using Main.Helper;

    public static class CaseScenarioSecond
    {
        internal static void ScenarioSecond()
        {
            // Step # | helper | method | value
            // 1 | Launch | id = com.pozitron.hepsiburada
            DriverHelper.LaunchApp();
            // 2 | Click | id = dod_all
            DriverHelper.ClickElementById(Element.DodAll);
            // 3 | Click | id = fl_product_item_image_container
            DriverHelper.ClickElementById(Element.ItemImageContainer);
            // 4 | Click | id = imageViewer
            DriverHelper.ClickElementById(Element.ImageViewer);
            // 5 | Swipe | id = imageViewer
            DriverHelper.Swipe(Element.ImageViewer,DriverHelper.Direction.Left);
            // 6 | Click | id = leftIcon
            DriverHelper.ClickElementById(Element.LeftIcon);
            // 7 | Click | id = leftIcon
            DriverHelper.ClickElementById(Element.LeftIcon);
            // 8 | Click | id = myList
            DriverHelper.ClickElementById(Element.Favourites);
            // 9 | SendKeys | id = txtUserName
            DriverHelper.SendElementById(Element.TxtUserName, Element.Mail);
            // 10 | Click | id = btnLogin
            DriverHelper.ClickElementById(Element.BtnLogin);
            // 11 | SendKeys | id = txtPassword
            DriverHelper.SendElementById(Element.TxtPassword, Element.Password);
            // 12 | Click | id = btnEmailSelect
            DriverHelper.ClickElementById(Element.BtnEmailSelect);
            // 13 | Click | XPath = //*[@text=\"Beğendiklerim\"]
            DriverHelper.ClickElementByXPath(Element.Favorites);
            // 14 | Click | id = touch_outside
            DriverHelper.ClickElementById(Element.Outside);
            // 15 | Assert | id = message
            Assert.That(DriverHelper.GetElementText(Element.LoginMessage),Is.EqualTo(Element.LoginMessageText));
            // 16 | Click | id = touch_outside
            DriverHelper.ClickElementById(Element.LoginButtonOk);
            // 17 | Click | id = leftIcon
            DriverHelper.ClickElementById(Element.LeftIcon);
            // 18 | Click | AccessibilityId = Listelerim
            DriverHelper.ClickElementByAId(Element.Lists);
            // 19 | Click | AccessibilityId = Beğendiklerim
            DriverHelper.ClickElementByAId(Element.Liked);
            // 20 | Assert | XPath = //*[@text=\"1 ürün\"]
            Assert.That(DriverHelper.GetElementText(Element.LikedText).Split(' ')[0],Is.EqualTo("1"));
            Assert.Pass();
        }
    }
}

