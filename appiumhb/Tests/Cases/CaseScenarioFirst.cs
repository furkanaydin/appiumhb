namespace appiumhb.Tests.Cases
{
    using Appiumhb.Main.Resources;
    using Main.Helper;
    using NUnit.Framework;

    public static class CaseScenarioFirst
    {
        internal static void ScenarioFirst()
        {
            // Step # | helper | method | value
            // 1 | Launch | id = com.pozitron.hepsiburada
            DriverHelper.LaunchApp();
            // 2 | Click | id = locationView
            DriverHelper.ClickElementById(Element.LocationView);
            // 3 | Click | id = citySelectorView
            DriverHelper.ClickElementById(Element.CitySelectorView);
            // 4 | Click | XPath = "//*[@text=\"Ağrı\"]"
            DriverHelper.ClickElementByXPath(Element.CitySelect);
            // 5 | Click | id = townSelectorView
            DriverHelper.ClickElementById(Element.TownSelectorView);
            // 6 | Click | XPath = "//*[@text=\"Hamur\"]"
            DriverHelper.ClickElementByXPath(Element.TownSelect);
            // 7 | Click | id = districtSelectorView
            DriverHelper.ClickElementById(Element.DistrictSelectorView);
            // 8 | Click | XPath = "//*[@text=\"Adımova Köyü\"]"
            DriverHelper.ClickElementByXPath(Element.DistrictSelect);
            // 9 | Click | id = buttonSave
            DriverHelper.ClickElementById(Element.ButtonSave);
            // 10 | Displayed | id = tvTitle
            Assert.That(DriverHelper.ElementIsDisplayed(Element.TvTitle), "Konumunuz kaydedilemedi.");
            // 11 | Click | id = nav_graph_category
            DriverHelper.ClickElementById(Element.NavGraphCategory);
            // 12 | Click | XPath = "//androidx.cardview.widget.CardView[3]"
            DriverHelper.ClickElementByXPath(Element.CardViewCategory);
            // 13 | Click | XPath = "//*[@text=\"iOS Telefonlar\"]"
            DriverHelper.ClickElementByXPath(Element.CategorySelect);
            // 14 | Text | id = "textViewLocation"
            Assert.That(DriverHelper.GetElementText(Element.TextViewLocation),Is.EqualTo(Element.City));
            Assert.Pass();
        }
    }
}

