namespace appiumhb.Tests.Fixtures
{
    using Cases;
    using Main.Helper;
    using NUnit.Framework;
    
    [TestFixture]
    [NonParallelizable]
    public class Tests
    {
        [OneTimeSetUp]
        public void Setup()
        {
            DriverHelper.CreateDriver(DriverHelper.DriverType.Android);
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            AppiumServer.StopLocalService();
        }
        
        [Test, Order(1),Description("It will regulate the scenarios to be continued with the location button on the application home page.")]
        public void TC_Scenario_First()
        {
            CaseScenarioFirst.ScenarioFirst();
        }

        [Test, Order(2),Description("The Super Price, Super Offer is created for the user to check the scenario created by clicking on any product.")]
        public void TC_Scenario_Second()
        {
            CaseScenarioSecond.ScenarioSecond();
        }
    }
}

