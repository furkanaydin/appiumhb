using System.Diagnostics;

namespace appiumhb.Main.Helper
{
    using System;
 using OpenQA.Selenium.Appium.Service;
    internal static class AppiumServer
    {
        private static AppiumLocalService? _localService;

        public static Uri LocalServiceUri
        {
            get
            {
                Debug.Assert(_localService != null, nameof(_localService) + " != null");
                if (!_localService.IsRunning)
                {
                    _localService.Start();
                }

                return _localService.ServiceUrl;
            }
        }
        public static void StopLocalService()
        {
            if (_localService is not {IsRunning: true}) return;
            _localService.Dispose();
        }
        public static Uri StartLocalService()
        {
            var localServerUri = new Uri("http://127.0.0.1:4723/wd/hub");
            return localServerUri;
        }
    }
}

