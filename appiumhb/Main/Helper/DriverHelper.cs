using OpenQA.Selenium;
using OpenQA.Selenium.Appium.MultiTouch;

namespace appiumhb.Main.Helper
{
    using System;
    using static DriverHelper.DriverType;
    using OpenQA.Selenium.Appium;
    using Appiumhb.Main.Resources;
    using OpenQA.Selenium.Appium.Android;
    using System.Diagnostics;
    using NUnit.Framework;
    using OpenQA.Selenium.Appium.iOS;
    public abstract class DriverHelper
    {
        private static AppiumDriver<AppiumWebElement>? Driver { get; set; }

        /// <summary>
        /// It's the method that uses the driverType to start the Appium server.
        /// </summary>
        /// <param name="driverType">Platform</param>
        /// <exception cref="ArgumentOutOfRangeException">The exception that is thrown when the value of an argument is outside the allowable range of values as defined by the invoked method.</exception>
        /// <example>
        /// DriverHelper.CreateDriver(DriverType.Android);
        /// </example>
        public static void CreateDriver(DriverType driverType)
        {
            AppiumOptions appiumOptions;
            Uri uri;
            switch (driverType)
            {
                case Ios:
                    appiumOptions = Capabilities.GetIosCapabilities();
                    uri = AppiumServer.StartLocalService();
                    Driver = new IOSDriver<AppiumWebElement>(uri, appiumOptions, InitTimeOutSec);
                    Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
                    Driver.LaunchApp();
                    break;
                case Android:
                    appiumOptions = Capabilities.GetAndroidCapabilitiesOldStyle();
                    uri = AppiumServer.StartLocalService();
                    Driver = new AndroidDriver<AppiumWebElement>(uri, appiumOptions, InitTimeOutSec);
                    Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(driverType), driverType, null);
            }
        }
        
        /// <summary>
        /// It's the method for detecting whether or not the Driver item is functional.
        /// </summary>
        /// <exception cref="NullReferenceException">The exception that is thrown when there is an attempt to dereference a null object reference.</exception>
        private static void CheckDriverValue()
        {
            if (Driver == null)
                throw new NullReferenceException(
                    "The exception that is thrown when there is an attempt to dereference a null object reference.");
        }

        /// <summary>
        /// Click element at its center point. Finds the first element matching the specified id.
        /// </summary>
        /// <param name="element">Unique identifier for a UI element</param>
        public static void ClickElementById(string element)
        {
            CheckDriverValue();
            Debug.Assert(Driver != null, nameof(Driver) + " != null");
            var webElement = Driver.FindElementById(element);
            webElement.Click();
            TestContext.WriteLine(element + "Clicked");
        }
        /// <summary>
        /// Click element at its center point. Finds the first of elements that match the Accessibility Id selector supplied
        /// </summary>
        /// <param name="element">Unique identifier for a UI element</param>
        public static void ClickElementByAId(string element)
        {
            CheckDriverValue();
            Debug.Assert(Driver != null, nameof(Driver) + " != null");
            var webElement = Driver.FindElementByAccessibilityId(element);
            webElement.Click();
            TestContext.WriteLine(element + "Clicked");
        }
        /// <summary>
        /// Click element at its center point. Finds the first element matching the specified XPath query.
        /// </summary>
        /// <param name="element">Unique identifier for a UI element</param>
        public static void ClickElementByXPath(string element)
        {
            CheckDriverValue();
            Debug.Assert(Driver != null, nameof(Driver) + " != null");
            var webElement = Driver.FindElementByXPath(element);
            webElement.Click();
            TestContext.WriteLine(element + " Clicked");
        }

        /// <summary>
        /// Send a sequence of key strokes to an element
        /// </summary>
        /// <param name="element">Unique identifier for a UI element</param>
        /// <param name="text">Any UTF-8 character</param>
        public static void SendElementById(string element, string text)
        {
            CheckDriverValue();
            Debug.Assert(Driver != null, nameof(Driver) + " != null");
            var webElement = Driver.FindElementById(element);
            webElement.SendKeys(text);
            TestContext.WriteLine($"Text {text} has been sent to the {element}.");
        }

        /// <summary>
        /// Gets a value indicating whether or not this element is displayed.
        /// </summary>
        /// <param name="element">Unique identifier for a UI element</param>
        /// <returns>Whether the element is displayed (boolean)</returns>
        public static bool ElementIsDisplayed(string element)
        {
            CheckDriverValue();
            Debug.Assert(Driver != null, nameof(Driver) + " != null");
            var webElement = Driver.FindElement(By.Id(element));
            bool isDisplayed = webElement.Displayed;
            return isDisplayed;
        }

        /// <summary>
        /// Gets the innerText of this element, without any leading or trailing whitespace, and with other whitespace collapsed.
        /// </summary>
        /// <param name="element">Unique identifier for a UI element</param>
        /// <returns>InnerText of this element</returns>
        public static string GetElementText(string element)
        {
            CheckDriverValue();
            Debug.Assert(Driver != null, nameof(Driver) + " != null");
            var webElement = Driver.FindElementById(element);
            string text = webElement.Text;
            return text;
        }

        /// <summary>
        /// Move to the specified location.
        /// Params:
        /// x – The x coordinate
        /// y – The y coordinate.
        /// </summary>
        /// <param name="element">Unique identifier for a UI element</param>
        /// <param name="direction"></param>
        public static void Swipe(string element,Direction direction)
        {
            TestContext.WriteLine("Scroll on the: " + element);
            CheckDriverValue();
            Debug.Assert(Driver != null, nameof(Driver) + " != null");
            var webElement = Driver.Manage().Window.Size;
            double elementHeight = webElement.Height;
            double elementWidth = webElement.Width;
            TestContext.WriteLine("Element Size > Width: "+elementWidth+"-Height: " +elementHeight);
            var calcStartWidth = (elementWidth / 20);
            var calcHeight = (elementHeight / 2);
            var calcEndWidth = (elementWidth / 10) * 9.5;
            switch (direction)
            {
                case Direction.Left:
                    new TouchAction(Driver).Press(calcEndWidth,calcHeight).MoveTo(calcStartWidth,calcHeight).Release().Perform();
                    TestContext.WriteLine("Element Scroll > StartX: "+calcEndWidth+ "StartY: " +calcHeight+"-EndX: " +calcStartWidth+"-EndY: " +calcHeight);
                    // Hesaplama ile gönderilen bilgisayarımda donmaya sebep oluyordu sonrasında eski usül ile denedim.
                    (new TouchAction(Driver))
                        .Press(1000, y: 1000)
                        .MoveTo(100, 1000)
                        .Release()
                        .Perform();
                    break;
                case Direction.Right:
                    new TouchAction(Driver).Press(calcStartWidth,calcHeight).MoveTo(calcEndWidth,calcHeight).Release().Perform();
                    TestContext.WriteLine("Element Scroll > StartX: "+calcStartWidth+ "StartY: " +calcHeight+"-EndX: " +calcEndWidth+"-EndY: " +calcHeight);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, "not supported.");
            }
        }

        public enum Direction
        {
            Left,
            Right
        }
        /// <summary>
        /// Launches the current app.
        /// </summary>
        public static void LaunchApp()
        {
            Debug.Assert(Driver != null, nameof(Driver) + " != null");
            Driver.LaunchApp();
        }
        public enum DriverType
        {
            Ios,
            Android
        }

        private static readonly TimeSpan InitTimeOutSec = TimeSpan.FromSeconds(180);
    }
}

