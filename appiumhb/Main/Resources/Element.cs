namespace Appiumhb.Main.Resources
{
    public struct Element
    {
        public const string City = "Adana";
        private const string Town = "Çukurova";
        private const string District = "Belediye Evleri"; 
        // First Scenario
        internal const string LocationView = "com.pozitron.hepsiburada:id/locationView";
        internal const string CitySelectorView = "com.pozitron.hepsiburada:id/citySelectorView";
        internal const string CitySelect = $"//*[@text=\"{City}\"]";
        internal const string TownSelectorView = "com.pozitron.hepsiburada:id/townSelectorView";
        internal const string TownSelect = $"//*[@text=\"{Town}\"]";
        internal const string DistrictSelectorView = "com.pozitron.hepsiburada:id/districtSelectorView";
        internal const string DistrictSelect = $"//*[@text=\"{District}\"]";
        internal const string ButtonSave = "com.pozitron.hepsiburada:id/buttonSave";
        internal const string TvTitle = "com.pozitron.hepsiburada:id/tvTitle";
        internal const string NavGraphCategory = "com.pozitron.hepsiburada:id/nav_graph_category";
        internal const string CardViewCategory = "//androidx.cardview.widget.CardView[3]";
        internal const string CategorySelect = "//*[@text=\"iOS Telefonlar\"]";
        internal const string TextViewLocation = "com.pozitron.hepsiburada:id/textViewLocation";
        // Second Scenario
        internal const string DodAll = "com.pozitron.hepsiburada:id/dod_all";
        internal const string ItemImageContainer = "com.pozitron.hepsiburada:id/fl_product_item_image_container";
        internal const string ImageViewer = "com.pozitron.hepsiburada:id/imageViewer";
        internal const string LeftIcon = "com.pozitron.hepsiburada:id/leftIcon";
        internal const string Favourites = "com.pozitron.hepsiburada:id/product_detail_favourites";
        internal const string TxtUserName = "txtUserName";
        internal const string BtnLogin = "btnLogin";
        internal const string TxtPassword = "txtPassword";
        internal const string BtnEmailSelect = "btnEmailSelect";
        internal const string Favorites = "//*[@text=\"Beğendiklerim\"]";
        internal const string Outside = "com.pozitron.hepsiburada:id/touch_outside";
        internal const string LoginMessage = "android:id/message";
        internal const string LoginMessageText = "Hoş geldin Furkan Aydın. Keyifli alışverişler dileriz.";
        internal const string LoginButtonOk = "android:id/button1";
        internal const string Lists = "Listelerim";
        internal const string Liked = "Bana Özel • 1 ürün Beğendiklerim Beğendiklerim";
        internal const string LikedText = "//*[@text=\"1 ürün\"]";
        //Login Credentials - My Temp Mail Address
        internal const string Mail = "hadate.maluken@yahoo.com";
        internal const string Password = "Hadate123.";
    }
}

