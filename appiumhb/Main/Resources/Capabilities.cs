namespace Appiumhb.Main.Resources
{
    using OpenQA.Selenium.Appium;
 using OpenQA.Selenium.Appium.Enums;
    /// <summary>
    /// Desired Capabilities are keys and values encoded in a JSON object, sent by Appium clients to the server when a new automation session is requested. They tell the Appium drivers all kinds of important things about how you want your test to work.
    /// </summary>
    public static class Capabilities
    {
        /// <summary>
        /// There are some issues with the new methods added to c# framework, but these issues go away when the old technique is utilized.
        /// </summary>
        /// <returns>Appium Client Builds Capabilities</returns>
        public static AppiumOptions GetAndroidCapabilitiesOldStyle()
        {
            var capabilities = new AppiumOptions();
            capabilities.AddAdditionalCapability("platformName", "Android");
            capabilities.AddAdditionalCapability("appium:platformVersion", "11");
            capabilities.AddAdditionalCapability("appium:deviceName", "Pixel3");
            capabilities.AddAdditionalCapability("appium:automationName","UIAutomator2");
            capabilities.AddAdditionalCapability("appium:app","/Users/furkanaydin/RiderProjects/appiumhb/appiumhb/appiumhb/Main/Resources/com.pozitron.hepsiburada.apk");
            capabilities.AddAdditionalCapability("appPackage", "com.pozitron.hepsiburada");
            capabilities.AddAdditionalCapability("appWaitActivity", "*");
            capabilities.AddAdditionalCapability("orientation", "PORTRAIT");
            capabilities.AddAdditionalCapability("noReset", true);
            capabilities.AddAdditionalCapability("fullReset", false);
            return capabilities;
        }

        public static AppiumOptions GetAndroidCapabilitiesNewStyle()
        {
            var capabilities = new AppiumOptions();
            capabilities.AddAdditionalCapability(MobileCapabilityType.PlatformName, "Android");
            capabilities.AddAdditionalCapability(MobileCapabilityType.PlatformVersion, "11");
            capabilities.AddAdditionalCapability(MobileCapabilityType.DeviceName, "Pixel3");
            capabilities.AddAdditionalCapability(MobileCapabilityType.AutomationName,"UIAutomator2");
            capabilities.AddAdditionalCapability(MobileCapabilityType.App,"/Users/furkanaydin/RiderProjects/appiumhb/appiumhb/appiumhb/Main/Resources/com.pozitron.hepsiburada.apk");
            capabilities.AddAdditionalCapability(AndroidMobileCapabilityType.AppPackage, "com.pozitron.hepsiburada");
            capabilities.AddAdditionalCapability(AndroidMobileCapabilityType.AppWaitActivity, "*");
            capabilities.AddAdditionalCapability(MobileCapabilityType.Orientation, "PORTRAIT");
            capabilities.AddAdditionalCapability(MobileCapabilityType.NoReset, true);
            capabilities.AddAdditionalCapability(MobileCapabilityType.FullReset, false);
            return capabilities;
        }

        public static AppiumOptions GetIosCapabilities()
        {
            var capabilities = new AppiumOptions();
            capabilities.AddAdditionalCapability(MobileCapabilityType.PlatformName, "iOS");
            capabilities.AddAdditionalCapability(MobileCapabilityType.PlatformVersion, "15.0");
            capabilities.AddAdditionalCapability(MobileCapabilityType.DeviceName, "iPhone 11");
            capabilities.AddAdditionalCapability(MobileCapabilityType.AutomationName,"XCuiTest");
            //I left it like way because I couldn't get to the ios program's application file. It can be given as a path once you compress the file with the app extension here.
            capabilities.AddAdditionalCapability(MobileCapabilityType.App,"/Users/furkanaydin/RiderProjects/appiumhb/appiumhb/appiumhb/Main/Resources/com.pozitron.hepsiburada.app.zip");
            return capabilities;
        }
    }
}

