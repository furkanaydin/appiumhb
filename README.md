# Hepsiburada Appium Test Automation


## Getting started

It is an example of a UI test automation project of the Hepsiburada app for iOS and Android using Appium frameworks.

## Concerning the Project 
Here is a list of suggested next steps to make it easier for you to get started with this project.

- [ ] To begin, clone the project to your own computer and open it in Visual Studio, Rider or etc.
- [ ] Get the nuget packages you'll need for the project. 
- [ ] It is our main test file for you to perform the appropriate tests in the project folder 'Appium Test.cs' under Fixtures under Tests. Alternatively, you can start the tests in Test Explorer.

```
git clone https://gitlab.com/furkanaydin/appiumhb.git [ or ] Download source code
```

## Used Technologies

#### appiumhb requires;
- [ ] [Microsoft.NET.Test.Sdk](https://www.nuget.org/packages/Microsoft.NET.Test.Sdk)
- [ ] [NUnit](https://www.nuget.org/packages/NUnit)
- [ ] [NUnit3TestAdapter](https://www.nuget.org/packages/NUnit3TestAdapter)
- [ ] [Appium.WebDriver](https://www.nuget.org/packages/Appium.WebDriver)
- [ ] [coverlet.collector](https://www.nuget.org/packages/coverlet.collector) : For Appium Dependencies
- [ ] [Castle.Core](https://www.nuget.org/packages/Castle.Core) : For Appium Dependencies
- [ ] [Newtonsoft.Json](https://www.nuget.org/packages/Newtonsoft.Json) : For Appium Dependencies


***
## Authors and acknowledgment
Furkan AYDIN

## Project status
The project has already been completed for preview, but further development on the iOS platform can be done, and the server and client-side methods can be improved.

## License
MIT License

Copyright (c) [2022] [appiumhb]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
